<?php declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    private int $minPagesNumber;

    private array $libraryBooks;

    private int $maxPrice;

    private array $storeBooks;

    /**
     * BooksGenerator constructor.
     *
     * @param int $minPagesNumber
     * @param array $libraryBooks
     * @param int $maxPrice
     * @param array $storeBooks
     */
    public function __construct(
        int $minPagesNumber,
        array $libraryBooks,
        int $maxPrice,
        array $storeBooks
    ) {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    /**
     * @return \Generator
     */
    public function generate(): \Generator
    {
        $libraryBooks = $this->libraryBooks;

        foreach ($libraryBooks as $book) {
            if ($book->getPagesNumber() >= $this->minPagesNumber) {
                yield $book;
            }
        }

        $storeBooks = $this->storeBooks;

        foreach ($storeBooks as $book) {
            if ($book->getPrice() <= $this->maxPrice) {
                yield $book;
            }
        }
    }
}