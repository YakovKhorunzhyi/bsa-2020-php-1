<?php declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $cars = $track->all();
        $str = '';

        foreach ($cars as $car) {
            $str .= "<p>{$car->getName()}: {$car->getSpeed()}, {$car->getPitStopTime()}</p>";
            $str .= "<p><img src=\"{$car->getImage()}\"><p>";
        }

        return $str;
    }
}