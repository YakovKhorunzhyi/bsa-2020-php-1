<?php declare(strict_types=1);

namespace App\Task1;

class Track
{
    private float $lapLength;

    private int $lapsNumber;

    private array $cars;

    /**
     * Track constructor.
     *
     * @param float $lapLength
     * @param int $lapsNumber
     *
     * @throws \Exception
     */
    public function __construct(float $lapLength, int $lapsNumber)
    {
        if ($lapLength < 0) {
            throw new \Exception('negative lap length');
        }

        if ($lapsNumber < 0) {
            throw new \Exception('negative laps number');
        }

        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    /**
     * @return float
     */
    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    /**
     * @return int
     */
    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    /**
     * @param Car $car
     */
    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function all(): array
    {
        if (empty($this->cars)) {
            throw new \Exception('Typed property App\Task1\Track::$cars must not be accessed before initialization');
        }

        return $this->cars;
    }

    /**
     * @return Car
     * @throws \Exception
     */
    public function run(): Car
    {
        if (is_null($this->cars)) {
            throw new \Exception('Typed property App\Task1\Track::$cars must not be accessed before initialization');
        }

        $cars = $this->cars;
        $winner = array_pop($cars);

        foreach ($cars as &$car) {
            $winner = $this->getWinner($winner, $car);
        }

        return $winner;
    }

    /**
     * @param Car $car1
     * @param Car $car2
     *
     * @return Car
     */
    private function getWinner(Car $car1, Car $car2): Car
    {
        return $this->passTimeForCar($car1) > $this->passTimeForCar($car2) ? $car2 : $car1;
    }

    /**
     * @param Car $car
     *
     * @return float|int
     */
    private function passTimeForCar(Car $car)
    {
        $fullLength = $this->lapLength * $this->lapsNumber;

        $fullFuelConsumption = ($fullLength / 100) * $car->getFuelConsumption();
        $needRefueling = ceil($fullFuelConsumption / $car->getFuelTankVolume());

        $refuelingTime = $needRefueling * $car->getPitStopTime();
        $totalTimeInSeconds = ($fullLength / $car->getSpeed()) * 60 * 60;

        return $refuelingTime + $totalTimeInSeconds;
    }
}